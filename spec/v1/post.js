import * as yup from "yup";

export default yup.object().shape({
    title: yup.string().label("Title").min(4).max(100).required("Title is required"),
    content: yup.string().label("Content").min(32).max(10000).required("Every post must have some content!"),
    recaptcha: yup.string().label("Recaptcha").required("You must solve the reCaptcha in order to post"),
    identity: yup.string().label("Identity").test({
        name: "isIdentityValid",
        test: val => {
            if(val && val.length)
                return val.length >= 12;
            
            return true;
        },
        message: "Identities have a minimum length of 12 characters"
    }),
    
    foregroundColor: yup.string().matches(/^#[a-f\d]{3}([a-f\d]{3})?$/i).required("Invalid foreground color"),
    backgroundColor: yup.string().matches(/^#[a-f\d]{3}([a-f\d]{3})?$/i).required("Invalid background color"),
    textColor: yup.string().matches(/^#[a-f\d]{3}([a-f\d]{3})?$/i).required("Invalid text color"),
    linkColor: yup.string().matches(/^#[a-f\d]{3}([a-f\d]{3})?$/i).required("Invalid link color"),
    quoteColor: yup.string().matches(/^#[a-f\d]{3}([a-f\d]{3})?$/i).required("Invalid quote color"),
    codeColor: yup.string().matches(/^#[a-f\d]{3}([a-f\d]{3})?$/i).required("Invalid code color"),
    accentColor: yup.string().matches(/^#[a-f\d]{3}([a-f\d]{3})?$/i).required("Invalid accent color"),

    narrow: yup.boolean().default(false).required("Narrow Post boolean required"),
    autoIndent: yup.boolean().default(false).required("Auto-Indent boolean required")
});