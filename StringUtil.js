export const pluralize = (n = n, singular, plural) =>
    n === 1 ? singular : (!!plural ? plural : singular + "s");