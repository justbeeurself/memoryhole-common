export const DEFAULT_COLORS = {
    text: "#212121",
    foreground: "#fafafa",
    background: "#424242",
    link: "#1e88e5",
    accent: "#f50057",
    quote: "#616161",
    code: "#f5f5f5"
};

export const parseColor = (color,defaultColor) =>
    (color && /^#[a-f\d]{3}([a-f\d]{3})?$/i.test(color)) ? color : defaultColor;

    export const parseColors = colors => ({
    text: parseColor(colors.textColor, DEFAULT_COLORS.text),
    foreground: parseColor(colors.foregroundColor, DEFAULT_COLORS.foreground),
    background: parseColor(colors.backgroundColor, DEFAULT_COLORS.background),
    link: parseColor(colors.linkColor, DEFAULT_COLORS.link),
    accent: parseColor(colors.accentColor, DEFAULT_COLORS.accent),
    quote: parseColor(colors.quoteColor, DEFAULT_COLORS.quote),
    code: parseColor(colors.codeColor, DEFAULT_COLORS.code)
});